var nunjucks = require('nunjucks');
var moment = require('moment');
var pinboard = require('pinboard');

var config = {
  lang: 'pt-BR',
  target: './temp/',
  sources: {
    posts: './posts/*.md', 
    less: './app/styles/**/*.less'
  },
  template: {
    dir: 'app/templates'
  },
  pinboard: {
    username: 'sigmus',
    password: 'xarope32',
    format: 'json' 
  },
  rsync: {
    args: ['--verbose'],
    exclude: ['.git*', 'node_modules'],
    recursive: true,
    host: 'sigmus@do.xpto.me',
    src: './temp',
    dest: '/home/sigmus/sites/main/'
  }
};

nunjucks.configure(config.template.dir);
moment.lang(config.lang);
pinboard.config(config.pinboard);

module.exports = config;
