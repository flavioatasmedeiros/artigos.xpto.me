var es = require('event-stream');
var frontMatter = require('front-matter');

module.exports = function(processAttrs, stripAttrs) {
  
  processAttrs = processAttrs || function(attrs, file) {
   file.attrs = attrs;
  };
  
  stripAttrs = typeof stripAttrs === 'undefined' ? true : stripAttrs;

  return es.map(function (file, cb) {
    
    var result = null;

    if (file.isNull()) { 
      return cb(null, file);
    }

    if (file.isStream()) { 
      return cb(new Error("gfront-matter: Streaming not supported")) ;
    }
    
    if (file.isBuffer()) {
      result = frontMatter(String(file.contents));
      
      processAttrs(result.attributes, file);
      
      if (stripAttrs) {
        file.contents = new Buffer(result.body);
      }
    }

    cb(null, file);
  });

};
