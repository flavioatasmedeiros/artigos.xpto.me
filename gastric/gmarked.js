var es = require('event-stream');
var gutil = require('gulp-util');
var marked = require('marked');

module.exports = function(opts) {
  
  opts = opts || {};

  return es.map(function (file, cb) {
    
    if (file.isNull()) { 
      return cb(null, file);
    }

    if (file.isStream()) { 
      return cb(new Error("gmarked: Streaming not supported")) ;
    }
    
    if (file.isBuffer()) {
      file.contents = new Buffer(marked(String(file.contents)));
    }

    file.path = gutil.replaceExtension(file.path, '.html');

    cb(null, file);
  });
};