var gulp = require('gulp');
var fs = require('fs');
var es = require('event-stream');
var mkdirp = require('mkdirp');

var config = require('./config');

var through = function(func) {
  return function(file) {
    return es.map(function (file, cb) {
      func(file);
      cb(null, file);
    });
  };
};

var createFile = function(name, data) {
  mkdirp(config.target, function (err) {
    if (err) throw err;
    fs.writeFile(config.target + '/' + name, data ,function (err) {
      if (err) throw err;
    });
  });
};

var registerTasks = function(tasks, list) {
  list.forEach(function(task) {
    gulp.task(task, tasks[task])
  });
};

module.exports = {
  through: through,
  createFile: createFile,
  registerTasks: registerTasks
};
