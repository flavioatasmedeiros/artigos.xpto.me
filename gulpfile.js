var _ = require('lodash');
var gulp = require('gulp');
var requireDir = require('require-dir');

var util = require('./gastric/util');
var config = require('./gastric/config');
var tasks = requireDir('./tasks');

util.registerTasks(tasks, ['home', 'posts', 'less', 'pinboard']);

gulp.task('generate', ['home', 'posts', 'less']);

gulp.task('deploy', ['rsync']);

gulp.task('build', ['generate'], tasks.rsync);

gulp.task('default', ['generate']);

gulp.task('watch', ['generate'], function() {
  gulp.watch(_.values(config.sources), ['generate']);
})
