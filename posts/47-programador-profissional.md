---
title: O Programador Profissional
slug: programador-profissional
date: 2014-03-02 10:00:00
published: true
---
<br>Adaptado de *[The Professional Programmer](http://programmer.97things.oreilly.com/wiki/index.php/The_Professional_Programmer)* — O que é um programador profissional?

O traço mais importante de um programador profissional é **responsabilidade profissional**. Programadores profissionais são responsáveis por suas carreiras, suas estimativas, seus compromissos, seus erros, pela qualidade do seu trabalho. Um programador profissional não se esquiva e passa essa responsabilidade para os outros.

Se e você é um profissional então você é responsável por sua própria carreira. **Você é responsável por ler e aprender**. Você é responsável por se manter atualizado com a indústria e com a tecnologia. Muitos programadores acham que é responsabilidade do empregador treiná-los. Desculpe, mas isso é definitivamente errado. 

Você acha que médicos se comportam dessa maneira? Você acha que advogados se comportam dessa maneira? Não, eles estudam usando seu próprio tempo, com seu próprio dinheiro. Gastam muitas das suas horas livres lendo jornais e decisões. Mantém-se atualizados. E assim deve ser com você. A relação entre você e o seu empregador é especificada de forma clara no contrato: ele promete pagar e você promete fazer um bom trabalho.

Profissionais assumem responsabilidade pelo código que escrevem. Eles não liberam código antes de saber que ele funciona. Pense nisso por um minuto. Como você pode se considerar um profissional se você está disposto a liberar código sem a certeza de que ele funciona? **Programadores profissionais esperam que o QA não encontre nada**, porque não liberam código até terem testado de forma exaustiva. É claro que o QA vai encontrar alguns problemas porque ninguém é perfeito. Mas como profissionais nossa atitude deve ser: o QA não vai encontrar nada.

Profissionais sabem trabalhar em grupo. Assumem responsabilidade pelo resultado do time inteiro, não apenas do seu trabalho. Eles se ajudam, ensinam um ao outro, aprendem entre si. Quando um companheiro precisa ajuda o outro comparece, sabendo que um dia pode ser ele que precise de ajuda.

Profissionais não toleram bagunça. Possuem orgulho pelo seu trabalho. Mantém seu código limpo, bem estruturado, fácil de ler. Seguem os padrões e melhores práticas acordadas. Nunca, nunca se apressam. Imagine que você esteja tendo uma experiência extracorpórea e esteja assistindo um médico realizando uma cirurgia de coração em você. Este médico tem um prazo. Ele deve terminar antes que a máquina que o mantém vivo estrague muitas células do seu sangue. Como você quer que ele se comporte? Você quer que ele se comporte como um típico desenvolvedor de software, apressado e fazendo bagunça? Você quer que ele diga “depois eu volto e arrumo isso”? Ou você quer que ele siga a prática correta, com calma, confiante que seu método é o melhor método que pode ser utilizado naquela situação. Você quer bagunça ou profissionalismo?

Profissionais são responsáveis. Assumem responsabilidade por suas carreiras. Assumem responsabilidade pelo correto funcionamento do seu código. Assumem responsabilidade pela qualidade de seu trabalho. Não abandonam seus princípios quando o prazo se esgota. Na verdade, quando a pressão vem, profissionais usam ainda mais as práticas que sabem ser corretas.



