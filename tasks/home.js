var nunjucks = require('nunjucks');

var util = require('../gastric/util');
var config = require('../gastric/config');
var post = require('./post');

var posts = []; 

var shouldPublish = function(post) {
  return (config.env === 'dev') ||
    (config.env === 'prod' && post.published);
};

var add = util.through(function(file) {
  posts.push(file.attrs);
  // if (shouldPublish(file.attrs)) {
  // }
});

var renderAndCreate = function() {
  var html = nunjucks.render('home.html', {posts: posts});
  util.createFile('index.html', html);
};

module.exports = function() {
  post.process(config.sources.posts)
    .pipe(add())
    .on('end', renderAndCreate);

};
