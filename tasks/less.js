var gulp = require('gulp');
var less = require('gulp-less');
var config = require('../gastric/config');

module.exports = function () {
  gulp.src(config.sources.less)
    .pipe(less({compress: false}))
    .pipe(gulp.dest('./temp/styles'));
};