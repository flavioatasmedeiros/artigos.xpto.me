var _ = require('lodash');
var pinboard = require('pinboard');

var toObject = function(data) {
  return _.map(data, function(count, tag) {
    return {
      name: tag,
      count: parseInt(count)
    };
  });
};

var sortByCount = _.partialRight(_.sortBy, 'count');

var logData = function(data) {
  console.log(data);
};

var reverse = function(data) {
  return data.reverse();
};

var transformTags = _.compose(reverse, sortByCount, toObject);

module.exports = function() {
  
  pinboard.get('tags/get', _.compose(logData, transformTags));

};
