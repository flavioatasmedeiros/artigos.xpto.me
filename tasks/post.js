var _ = require('lodash');
var gulp = require('gulp');
var moment = require('moment');

var gmarked = require('../gastric/gmarked');
var frontMatter = require('../gastric/gfront-matter');
var util = require('../gastric/util.js');

var formatDate = util.through(function(file) {
  var date = file.attrs.date;
  file.attrs.when = moment(date).format('LL');
  file.attrs.path = moment(date).format('YYYY/MM/') + file.attrs.slug;
});

var contentsToAttrs = util.through(function(file) {
  _.extend(file.attrs, {
    html: file.contents
  });
});

module.exports = { 
  process: function(sources) {
    return gulp.src(sources)
      .pipe(frontMatter())
      .pipe(gmarked())
      .pipe(formatDate())
      .pipe(contentsToAttrs());
  } 
};
