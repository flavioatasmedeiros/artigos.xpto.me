var gulp  = require('gulp');
var nunjucks = require('nunjucks');

var config = require('../gastric/config');
var util  = require('../gastric/util');
var post  = require('./post');

var renderTemplate = util.through(function(file) {
  file.contents = new Buffer(
    nunjucks.render('post.html', file.attrs)
  );
});

var changePath = util.through(function(file) {
  file.path = file.attrs.path + '/index.html';
});

module.exports = function() {
  return post.process(config.sources.posts)
    .pipe(renderTemplate())
    .pipe(changePath())
    .pipe(gulp.dest(config.target + '/**'));
};
